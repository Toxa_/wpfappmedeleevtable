﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppMendeleevTable
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static global::System.Resources.ResourceManager resourceMan;

        private static global::System.Globalization.CultureInfo resourceCulture;

        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]

        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WpfAppMendeleevTable.ChimElem", typeof(ChimElem).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }

        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }
        public MainWindow()
        {
            InitializeComponent();
        }

        string path;
        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            foreach (var item in mainGrid.Children)
            {
                if (item is Border)
                {
                    if (((Border)item).IsMouseOver)
                    {
                        //((TextBlock)((Grid)((Border)Popup.Child).Child).Children[1]).Text = ((TextBlock)((Grid)((Border)item).Child).Children[2]).Text;

                        var nameElem = ((TextBlock)((Grid)((Border)item).Child).Children[2]).Text;
                        var childrens = ResourceManager.GetString(nameElem, resourceCulture);

                        if (childrens != null && childrens!="")
                        {
                            var childrens2 = childrens.Split('$');

                            imagePopup.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(childrens2[0]);
                            mainText.Text = childrens2[1];

                            path = ("https://en.wikipedia.org/wiki/" + nameElem);
                            hyper.Inlines.Clear();
                            hyper.Inlines.Add(((TextBlock)((Grid)((Border)item).Child).Children[2]).Text.ToString());


                            if (Popup.IsOpen) { Popup.IsOpen = false; return; }
                            if (!Popup.IsOpen) Popup.IsOpen = true;
                        }
                    }
                }
            }
        }

        private void Hyper_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(path));
        }

        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            foreach (var item in typeElementsGrid.Children)
            {
                if (item is TextBlock)
                {
                    if (((TextBlock)item).IsMouseOver)
                    {
                        var brush = ((TextBlock)item).Foreground;
                        foreach (var item2 in mainGrid.Children)
                        {
                            if (item2 is Border)
                            {
                                if (((Border)item2).Background.ToString() != brush.ToString())
                                {
                                    ((Border)item2).Opacity = 0.5;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void TextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            foreach (var item in mainGrid.Children)
            {
                if (item is Border)
                {
                    ((Border)item).Opacity = 1;
                }
            }
        }
    }
}